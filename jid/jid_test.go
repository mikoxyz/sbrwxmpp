// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package jid

import (
	"testing"
)

func TestParse(t *testing.T) {
	data := []struct {
		String string
		JID    JID
	}{
		{"test.example.com", JID{"", "test.example.com", ""}},
		{"user.123@test.example.com", JID{"user.123", "test.example.com", ""}},
		{"user.123@test.example.com/test@resource/hello", JID{"user.123", "test.example.com", "test@resource/hello"}},
		{"test.example.com/test@resource/hello", JID{"", "test.example.com", "test@resource/hello"}},
	}
	for _, r := range data {
		parsed := Parse(r.String)
		if parsed != r.JID {
			t.Errorf("Parsing %q gave %#v; expected %#v\n", r.String, parsed, r.JID)
		}
	}
}

func TestBareEqual(t *testing.T) {
	data := []struct {
		A      JID
		B      JID
		Result bool
	}{
		{JID{"user", "example.com", "Resource"}, JID{"user", "example.com", "Resource"}, true},
		{JID{"user", "example.com", "Resource1"}, JID{"user", "example.com", "Resource2"}, true},
		{JID{"user1", "example.com", "Resource"}, JID{"user2", "example.com", "Resource"}, false},
		{JID{"user", "test1.example.com", "Resource"}, JID{"user", "test2.example.com", "Resource"}, false},
	}

	for _, r := range data {
		result := BareEqual(r.A, r.B)
		if result != r.Result {
			t.Errorf("BareEqual(%v, %v) gave %v; expected %v\n", r.A, r.B, result, r.Result)
		}
	}
}
