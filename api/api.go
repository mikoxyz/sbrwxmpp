// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package api

import (
	"encoding/json"
	"encoding/xml"
	"net/http"
	_ "net/http/pprof"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"github.com/gorilla/mux"
	"github.com/redbluescreen/sbrwxmpp/config"
	"github.com/redbluescreen/sbrwxmpp/db"
	xmlstream "github.com/redbluescreen/sbrwxmpp/xmlstream2"
	"github.com/redbluescreen/sbrwxmpp/xmpp"
	"github.com/rs/zerolog"
)

type Server struct {
	XMPP   *xmpp.XmppServer
	DB     *db.DB
	Config *config.Config
	Logger zerolog.Logger
}

func (s Server) promRegistry(cs ...prometheus.Collector) *prometheus.Registry {
	r := prometheus.NewRegistry()
	r.MustRegister(prometheus.NewBuildInfoCollector())
	r.MustRegister(prometheus.NewGoCollector())
	r.MustRegister(prometheus.NewProcessCollector(prometheus.ProcessCollectorOpts{}))
	r.MustRegister(prometheus.NewGaugeFunc(prometheus.GaugeOpts{
		Name: "active_clients",
		Help: "Number of active clients.",
	}, func() float64 {
		s.XMPP.Lock()
		defer s.XMPP.Unlock()
		return float64(len(s.XMPP.Clients))
	}))
	r.MustRegister(prometheus.NewGaugeFunc(prometheus.GaugeOpts{
		Name: "active_rooms",
		Help: "Number of active chat rooms.",
	}, func() float64 {
		s.XMPP.Lock()
		defer s.XMPP.Unlock()
		return float64(len(s.XMPP.Rooms))
	}))
	r.MustRegister(cs...)
	return r
}

func (s Server) Run() {
	reqCounter := prometheus.NewCounterVec(prometheus.CounterOpts{
		Name: "api_requests_total",
		Help: "Total number of API requests by HTTP status code.",
	}, []string{"code"})

	mux := mux.NewRouter()
	mux.Handle("/metrics", promhttp.HandlerFor(s.promRegistry(reqCounter), promhttp.HandlerOpts{}))
	mux.HandleFunc("/api/sessions", s.getSessions).Methods("GET")
	mux.HandleFunc("/api/rooms", s.getRooms).Methods("GET")
	mux.HandleFunc("/api/users/{to}/message", s.sendMessage(false)).Methods("POST")
	mux.HandleFunc("/api/rooms/{to}/message", s.sendMessage(true)).Methods("POST")
	mux.HandleFunc("/api/users", s.upsertUser).Methods("POST")
	mux.HandleFunc("/api/users/{user}", s.deleteUser).Methods("DELETE")
	mux.HandleFunc("/api/users/{user}/kick", s.kickUser).Methods("POST")
	mux.PathPrefix("/debug/pprof/").Handler(http.DefaultServeMux)
	mux.Use(loggerMiddleware(s.Logger))
	mux.Use(authMiddleware(s.Config.API.Key))
	err := http.ListenAndServe(s.Config.API.Addr, promhttp.InstrumentHandlerCounter(reqCounter, mux))
	if err != nil {
		s.Logger.Fatal().Str("addr", s.Config.API.Addr).Err(err).Msg("Failed to listen")
	}
}

func (s Server) getSessions(rw http.ResponseWriter, r *http.Request) {
	s.XMPP.Lock()
	sessions := make([]string, 0, len(s.XMPP.Clients))
	for id := range s.XMPP.Clients {
		sessions = append(sessions, id)
	}
	s.XMPP.Unlock()
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(sessions)
}

func (s Server) getRooms(rw http.ResponseWriter, r *http.Request) {
	type roomInfo struct {
		Name    string   `json:"name"`
		Members []string `json:"members"`
	}
	s.XMPP.Lock()
	rooms := make([]roomInfo, 0, len(s.XMPP.Rooms))
	for _, room := range s.XMPP.Rooms {
		members := make([]string, 0, len(room.Members))
		for member := range room.Members {
			members = append(members, member.JID.Local)
		}
		rooms = append(rooms, roomInfo{
			Name:    room.ID,
			Members: members,
		})
	}
	s.XMPP.Unlock()
	rw.Header().Set("Content-Type", "application/json")
	json.NewEncoder(rw).Encode(rooms)
}

func (s Server) sendMessage(room bool) func(http.ResponseWriter, *http.Request) {
	return func(rw http.ResponseWriter, r *http.Request) {
		var body struct {
			From    string `json:"from"`
			Body    string `json:"body"`
			Subject string `json:"subject"`
		}
		err := json.NewDecoder(r.Body).Decode(&body)
		if err != nil {
			s.Logger.Error().Err(err).Msg("Error handling request")
			rw.WriteHeader(http.StatusBadRequest)
			return
		}
		el := xmlstream.Element{
			Name: xml.Name{
				Local: "message",
				Space: "jabber:client",
			},
			Children: []xmlstream.Element{
				{
					Name: xml.Name{
						Local: "body",
						Space: "jabber:client",
					},
					Text: body.Body,
				},
				{
					Name: xml.Name{
						Local: "subject",
						Space: "jabber:client",
					},
					Text: body.Subject,
				},
			},
		}
		el.SetAttr("from", body.From)
		var service string
		if room {
			service = "conference." + xmpp.DefaultDomain
		} else {
			service = xmpp.DefaultDomain
		}
		el.SetAttr("to", mux.Vars(r)["to"]+"@"+service)
		if room {
			el.SetAttr("type", "groupchat")
		}
		s.XMPP.RoutePluginMessage(el)
	}
}

func (s Server) upsertUser(rw http.ResponseWriter, r *http.Request) {
	var body struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}
	err := json.NewDecoder(r.Body).Decode(&body)
	if err != nil {
		s.Logger.Error().Err(err).Msg("Error handling request")
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
	if !jidNodeValid(body.Username) {
		rw.WriteHeader(http.StatusBadRequest)
		return
	}
	err = s.DB.UpsertUser(db.User{
		Name:     body.Username,
		Password: []byte(body.Password),
	})
	if err != nil {
		s.Logger.Error().Err(err).Msg("Error handling request")
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (s Server) deleteUser(rw http.ResponseWriter, r *http.Request) {
	err := s.DB.DeleteUser(mux.Vars(r)["user"])
	if err != nil {
		s.Logger.Error().Err(err).Msg("Error handling request")
		rw.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (s Server) kickUser(rw http.ResponseWriter, r *http.Request) {
	userID := mux.Vars(r)["user"]
	s.XMPP.Lock()
	client := s.XMPP.Clients[userID]
	if client != nil {
		client.CloseError("<not-authorized xmlns='urn:ietf:params:xml:ns:xmpp-streams'/>")
	}
	s.XMPP.Unlock()
}

func jidNodeValid(s string) bool {
	if len(s) == 0 || len(s) > 256 {
		return false
	}
	return !strings.ContainsAny(s, "\"&'/:<>@\u007F\uFFFE\uFFFF")
}
