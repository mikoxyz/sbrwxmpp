// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package xmpp

import (
	"fmt"

	"github.com/redbluescreen/sbrwxmpp/jid"
	xmlstream "github.com/redbluescreen/sbrwxmpp/xmlstream2"
)

type Room struct {
	roomsMap map[string]*Room
	ID       string
	Members  map[*Client]struct{}
}

// getRoomJid returns the JID for a room member specified by `from`
// for sending to a client with JID `to`
func (r *Room) getRoomJid(from jid.JID, to jid.JID) jid.JID {
	return jid.JID{
		Local:    r.ID,
		Domain:   "conference." + to.Domain,
		Resource: from.Local,
	}
}

func (r *Room) RouteMessage(msg xmlstream.Element) {
	fromJID := jid.Parse(msg.GetAttr("from"))
	for member := range r.Members {
		msg.SetAttr("from", r.getRoomJid(fromJID, member.JID).String())
		msg.SetAttr("to", member.JID.String())
		member.WriteQueue(msg)
	}
}

func (r *Room) AddMember(c *Client) {
	// Return early if the member is already in the room
	if _, hasMember := r.Members[c]; hasMember {
		return
	}

	// Send presence of the added member to all current members
	for member := range r.Members {
		str := "<presence from='%v' to='%v'>" +
			"<x xmlns='http://jabber.org/protocol/muc#user'>" +
			"<item affiliation='member' role='participant'/></x></presence>"
		member.WriteQueue(fmt.Sprintf(str, r.getRoomJid(c.JID, member.JID), member.JID))
	}

	// Add member to the members map
	r.Members[c] = struct{}{}

	// Send presences of all members to the new member
	for member := range r.Members {
		str := "<presence from='%v' to='%v'>" +
			"<x xmlns='http://jabber.org/protocol/muc#user'>" +
			"<item affiliation='member' role='participant'/>"
		if member == c {
			str += "<status code='110'/>"
		}
		str += "</x></presence>"
		c.WriteQueue(fmt.Sprintf(str, r.getRoomJid(member.JID, c.JID), c.JID))
	}
}

func (r *Room) RemoveMember(c *Client) {
	// Return early if the member isn't in the room
	if _, hasMember := r.Members[c]; !hasMember {
		return
	}

	// Send presence of removed member to all members
	for member := range r.Members {
		str := "<presence from='%v' to='%v' type='unavailable'>" +
			"<x xmlns='http://jabber.org/protocol/muc#user'>" +
			"<item affiliation='member' role='none'/>"
		if member == c {
			str += "<status code='110'/>"
		}
		str += "</x></presence>"
		member.WriteQueue(fmt.Sprintf(str, r.getRoomJid(c.JID, member.JID), member.JID))
	}

	// Delete member from members map
	delete(r.Members, c)

	// If the room is now empty, remove it from the Server's rooms map
	if len(r.Members) == 0 {
		delete(r.roomsMap, r.ID)
	}
}
