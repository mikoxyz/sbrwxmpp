// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package xmpp

import (
	"bytes"
	"encoding/xml"
	"fmt"
	"net"
	"runtime/debug"
	"sync/atomic"
	"time"

	"github.com/redbluescreen/sbrwxmpp/jid"
	"github.com/redbluescreen/sbrwxmpp/tls"
	xmlstream "github.com/redbluescreen/sbrwxmpp/xmlstream2"
	"github.com/rs/zerolog"
)

type Client struct {
	conn          net.Conn
	tlsConfig     *tls.Config
	authenticated bool
	stream        *xmlstream.ElementStream
	rootLogger    zerolog.Logger
	logger        zerolog.Logger
	JID           jid.JID
	server        *XmppServer
	streamClosed  uint32
	writeChan     chan string
	domain        string
}

func (c *Client) WriteRoutine() {
	for msg := range c.writeChan {
		c.WriteBlocking(msg)
	}
}

func (c *Client) isTLSConn() bool {
	_, ok := c.conn.(*tls.Conn)
	return ok
}

func (c *Client) CloseError(err interface{}) {
	if atomic.LoadUint32(&c.streamClosed) != 0 {
		// Stream is already closed
		return
	}

	// Give the client one second to end the stream
	c.conn.SetReadDeadline(time.Now().Add(1 * time.Second))
	c.WriteQueue("<stream:error>" + interfaceAsString(err) + "</stream:error></stream:stream>")
	atomic.StoreUint32(&c.streamClosed, 1) // Has to be after WriteQueue
}

func (c *Client) HandleConnection() {
	defer func() {
		if r := recover(); r != nil {
			ev := c.logger.Error()
			if err, ok := r.(error); ok {
				ev.Err(err)
			}
			ev.Bytes("stack", debug.Stack()).Msg("Panic handling connection")
		}
		c.server.RemoveClient(c)
		c.conn.Close()
		close(c.writeChan)
		c.logger.Info().Msg("Connection closed")
	}()
	stream, err := xmlstream.NewStream(c.conn)
	if err != nil {
		c.logger.Error().Err(err).Msg("Error creating XML stream")
		return
	}
	c.handleRootElement(stream)
	c.stream = stream
	for {
		e, err := c.stream.NextChild()
		if err == xmlstream.NoMoreChildrenError {
			c.logger.Debug().Msg("XML stream ended")
			if atomic.LoadUint32(&c.streamClosed) == 0 {
				c.WriteBlocking("</stream:stream>")
			}
			return
		}
		if err != nil {
			if v, ok := err.(net.Error); ok && v.Timeout() {
				// Read deadline reached. It means that we closed the stream and the client didn't respond in time.
				return
			}
			c.logger.Error().Err(err).Msg("Error getting next child")
			return
		}

		err = c.handleXMLElement(e)
		if err != nil {
			c.logger.Error().Err(err).Msg("Error handling element")
			return
		}
	}
}

func (c *Client) handleRootElement(e *xmlstream.ElementStream) {
	c.logger.Trace().Stringer("elem", e).Msg("Received stream start")
	c.domain = e.GetAttr("to")
	c.sendStreamStart()
	c.sendStreamFeatures()
}

func (c *Client) handleXMLElement(e xmlstream.Element) error {
	c.logger.Trace().Stringer("elem", e).Msg("Handle element")
	switch e.Name.Local {
	case "starttls":
		if !c.isTLSConn() {
			return c.doTLS()
		}
	case "iq":
		c.handleIq(e)
	case "presence":
		if c.authenticated {
			c.handlePresence(e)
		}
	case "message":
		if c.authenticated {
			c.handleMessage(e)
		}
	default:
		c.logger.Debug().Stringer("elem", e).Msg("Received unknown XML element")
	}
	return nil
}

func (c *Client) handleIq(e xmlstream.Element) {
	id := e.GetAttr("id")
	typ := e.GetAttr("type")
	if id == "" || (typ != "get" && typ != "set") {
		// TODO: return iq error
		c.logger.Warn().Msg("iq error: id or type invalid")
		return
	}
	for _, el := range e.Children {
		if el.Name.Local == "query" && el.Name.Space == "jabber:iq:auth" {
			c.logger.Debug().Msg("Received authentication IQ")
			if typ == "get" {
				s := "<iq type='result' id='%v'><query xmlns='jabber:iq:auth'><username/><password/><resource/></query></iq>"
				c.WriteBlocking(fmt.Sprintf(s, id))
			} else {
				uc, _ := el.GetChild("username")
				pc, _ := el.GetChild("password")
				rc, _ := el.GetChild("resource")
				user, err := c.server.DB.GetUser(uc.Text)
				if err != nil {
					c.logger.Error().Err(err).Msg("Error getting user")
					s := "<iq type='error' id='%v'><error type='cancel'>" +
						"<internal-server-error xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>" +
						"</error></iq>"
					c.WriteBlocking(fmt.Sprintf(s, id))
					continue
				}
				if !bytes.Equal(user.Password, []byte(pc.Text)) {
					s := "<iq type='error' id='%v'><error code='401' type='auth'>" +
						"<not-authorized xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/>" +
						"</error></iq>"
					c.WriteBlocking(fmt.Sprintf(s, id))
					continue
				}
				c.JID = jid.JID{
					Local:    uc.Text,
					Domain:   c.domain,
					Resource: rc.Text,
				}
				c.logger.UpdateContext(func(ctx zerolog.Context) zerolog.Context {
					return ctx.Str("jid", c.JID.String())
				})

				c.server.Lock()
				oldClient := c.server.Clients[c.JID.Local]
				if oldClient != nil {
					oldClient.logger.Warn().Msg("Kicking client because of JID conflict")
					oldClient.CloseError("<conflict xmlns='urn:ietf:params:xml:ns:xmpp-streams'/>")
				}
				c.server.Unlock()

				s := "<iq type='result' id='%v'/>"
				c.WriteBlocking(fmt.Sprintf(s, id))
				c.authenticated = true
			}
		} else {
			c.logger.Debug().Stringer("elem", e).Msg("Received unknown IQ")
			s := "<iq type='error' id='%v' from='%v'><error type='cancel'><service-unavailable xmlns='urn:ietf:params:xml:ns:xmpp-stanzas'/></error></iq>"
			c.WriteBlocking(fmt.Sprintf(s, id, c.domain))
		}
	}
}

func (c *Client) handlePresence(e xmlstream.Element) {
	to := e.GetAttr("to")
	typ := e.GetAttr("type")
	if to != "" {
		toLocal := jid.Parse(to).Local
		if typ == "" {
			c.logger.Debug().Str("room", toLocal).Msg("Groupchat join")

			c.server.Lock()
			room := c.server.getOrCreateRoom(toLocal)
			room.AddMember(c)
		} else if typ == "unavailable" {
			c.logger.Debug().Str("room", toLocal).Msg("Groupchat leave")

			c.server.Lock()
			room := c.server.Rooms[toLocal]
			if room == nil {
				c.logger.Warn().Str("room", toLocal).Msg("Tried to leave room that doesn't exist")
				c.server.Unlock()
				return
			}
			room.RemoveMember(c)
		}
		c.server.Unlock()
	} else {
		c.logger.Debug().Msg("Adding client to available clients")
		c.server.AddClient(c)
	}
}

func (c *Client) handleMessage(e xmlstream.Element) {
	e.SetAttr("from", c.JID.String())
	c.server.routeClientMessage(e)
}

func interfaceAsString(i interface{}) string {
	switch v := i.(type) {
	case string:
		return v
	case xmlstream.Element:
		return v.String()
	default:
		panic(fmt.Sprintf("type %T can't be sent to client", v))
	}
}

func (c *Client) WriteBlocking(i interface{}) {
	str := interfaceAsString(i)
	c.logger.Trace().Str("payload", str).Msg("Send")
	_ = c.conn.SetWriteDeadline(time.Now().Add(5 * time.Second))
	_, err := c.conn.Write([]byte(str))
	if err != nil {
		c.logger.Error().Err(err).Msg("Write error")
		c.conn.Close()
	}
}

func (c *Client) doTLS() error {
	c.WriteBlocking("<proceed xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>")
	// Reset write deadline after WriteBlocking
	_ = c.conn.SetWriteDeadline(time.Time{})
	tlsConn := tls.Server(c.conn, c.tlsConfig)
	if err := tlsConn.Handshake(); err != nil {
		return fmt.Errorf("tls handshake error: %v", err)
	}
	c.conn = tlsConn
	stream, err := xmlstream.NewStream(c.conn)
	if err != nil {
		return fmt.Errorf("error creating xml stream: %v", err)
	}
	c.handleRootElement(stream)
	c.stream = stream
	return nil
}

func (c *Client) sendStreamStart() {
	id := RandomStringSecure(10)
	c.logger = c.rootLogger.With().Str("stream", id).Logger()
	t := xml.Header + "<stream:stream " +
		"from='%v' " +
		"id='%v' " +
		"version='1.0' " +
		"xml:lang='en' " +
		"xmlns='jabber:client' " +
		"xmlns:stream='http://etherx.jabber.org/streams'>"
	c.WriteBlocking(fmt.Sprintf(t, c.domain, id))
}

func (c *Client) sendStreamFeatures() {
	var features string
	if !c.isTLSConn() {
		// Not talking over TLS yet, offer starting TLS connection
		features = "<starttls xmlns='urn:ietf:params:xml:ns:xmpp-tls'><required/></starttls>"
	} else {
		// Talking over TLS, offer iq-auth
		features = "<auth xmlns='http://jabber.org/features/iq-auth'/>"
	}
	c.WriteBlocking("<stream:features>" + features + "</stream:features>")
}

func (c *Client) WriteQueue(i interface{}) {
	if atomic.LoadUint32(&c.streamClosed) != 0 {
		return
	}
	str := interfaceAsString(i)
	select {
	case c.writeChan <- str:
	default:
		c.logger.Warn().Msg("Write dropped")
	}
}
