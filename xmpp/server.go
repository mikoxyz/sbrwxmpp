// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package xmpp

import (
	"net"
	"sync"

	"github.com/redbluescreen/sbrwxmpp/config"
	"github.com/redbluescreen/sbrwxmpp/db"
	"github.com/redbluescreen/sbrwxmpp/jid"
	"github.com/redbluescreen/sbrwxmpp/plugin"
	"github.com/redbluescreen/sbrwxmpp/tls"
	xmlstream "github.com/redbluescreen/sbrwxmpp/xmlstream2"
	"github.com/rs/zerolog"
)

type XmppServer struct {
	sync.Mutex
	Clients map[string]*Client
	Rooms   map[string]*Room
	Logger  zerolog.Logger
	Config  *config.Config
	DB      *db.DB
	Plugins []plugin.Plugin
}

func (s *XmppServer) Run(ln net.Listener, tlsConfig *tls.Config) {
	for {
		conn, err := ln.Accept()
		if err != nil {
			panic(err)
		}
		logger := s.Logger.With().Str("addr", conn.RemoteAddr().String()).Logger()
		logger.Info().Msg("Accepted TCP connection")

		cl := &Client{
			conn:       conn,
			tlsConfig:  tlsConfig,
			rootLogger: logger,
			logger:     logger,
			server:     s,
			writeChan:  make(chan string, s.Config.WriteQueueSize),
		}
		go cl.HandleConnection()
		go cl.WriteRoutine()
	}
}

func (s *XmppServer) getOrCreateRoom(id string) *Room {
	room := s.Rooms[id]
	if room != nil {
		return room
	}
	room = &Room{
		roomsMap: s.Rooms,
		ID:       id,
		Members:  make(map[*Client]struct{}),
	}
	s.Rooms[id] = room
	return room
}

func (s *XmppServer) RoutePluginMessage(msg xmlstream.Element) {
	s.routeMessageInternal(msg, true)
}

func (s *XmppServer) routeClientMessage(msg xmlstream.Element) {
	s.routeMessageInternal(msg, false)
}

func (s *XmppServer) routeMessageInternal(msg xmlstream.Element, byPlugin bool) {
	fromJID := jid.Parse(msg.GetAttr("from"))
	toJID := jid.Parse(msg.GetAttr("to"))
	body, ok := msg.GetChild("body")
	if ok {
		pluginMsg := &plugin.Message{
			From:      fromJID,
			To:        toJID,
			GroupChat: msg.GetAttr("type") == "groupchat",
			Body:      body.Text,
			ByPlugin:  byPlugin,
		}
		for _, plugin := range s.Plugins {
			plugin.OnMessage(pluginMsg)
			if pluginMsg.Cancelled {
				return
			}
		}
	}
	s.Logger.Debug().Stringer("msg", msg).Msg("Routing message")
	s.Lock()
	if msg.GetAttr("type") == "groupchat" {
		// Route the message to a room
		room := s.Rooms[toJID.Local]
		if room != nil {
			room.RouteMessage(msg)
		}
	} else {
		// Route the message to a specific client
		client := s.Clients[toJID.Local]
		if client != nil {
			msg.SetAttr("from", fromJID.WithDomain(client.JID.Domain).String())
			msg.SetAttr("to", toJID.WithDomain(client.JID.Domain).String())
			client.WriteQueue(msg)
		}
	}
	s.Unlock()
}

func (s *XmppServer) AddClient(c *Client) {
	s.Lock()
	s.Clients[c.JID.Local] = c
	s.Unlock()
}

func (s *XmppServer) RemoveClient(c *Client) {
	s.Lock()
	for _, room := range s.Rooms {
		room.RemoveMember(c)
	}
	delete(s.Clients, c.JID.Local)
	s.Unlock()
}
