module github.com/redbluescreen/sbrwxmpp

go 1.12

require (
	github.com/BurntSushi/toml v1.3.2
	github.com/bwmarrin/discordgo v0.27.1
	github.com/gorilla/mux v1.8.0
	github.com/mattn/go-colorable v0.1.13
	github.com/prometheus/client_golang v1.16.0
	github.com/rs/zerolog v1.29.1
	go.etcd.io/bbolt v1.3.7
	golang.org/x/crypto v0.10.0
)
