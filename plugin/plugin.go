// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package plugin

import (
	"github.com/redbluescreen/sbrwxmpp/jid"
)

type Message struct {
	From      jid.JID
	To        jid.JID
	GroupChat bool
	Body      string

	ByPlugin  bool
	Cancelled bool
}

type Plugin interface {
	OnMessage(*Message)
}
