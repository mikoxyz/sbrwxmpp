// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package discord

import (
	"encoding/xml"
	"fmt"
	"net/http"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/redbluescreen/sbrwxmpp/plugin"
	"github.com/redbluescreen/sbrwxmpp/plugin/registry"
	"github.com/redbluescreen/sbrwxmpp/sbrw"
	xmlstream "github.com/redbluescreen/sbrwxmpp/xmlstream2"
	"github.com/redbluescreen/sbrwxmpp/xmpp"
	"github.com/rs/zerolog"
)

func init() {
	registry.Register("discord", Create, 100)
}

type channel struct {
	DiscordID string
	RoomName  string
	Webhook   *discordgo.Webhook
}

func findOrCreateWebhook(dg *discordgo.Session, channel string) (*discordgo.Webhook, error) {
	webhooks, err := dg.ChannelWebhooks(channel)
	if err != nil {
		return nil, err
	}
	for _, webhook := range webhooks {
		if webhook.Name == "ChatSync" {
			return webhook, nil
		}
	}
	return dg.WebhookCreate(channel, "ChatSync", "")
}

func Create(srv *xmpp.XmppServer, logger zerolog.Logger) (plugin.Plugin, error) {
	var config Config
	if err := srv.Config.DecodePluginConfig("discord", &config); err != nil {
		return nil, err
	}
	p := &Plugin{
		log: logger,
		cfg: config,
		srv: srv,
	}

	if p.cfg.Enabled {
		if err := p.initialize(); err != nil {
			return nil, err
		}
	}

	return p, nil
}

type Plugin struct {
	log      zerolog.Logger
	cfg      Config
	dg       *discordgo.Session
	channels []channel
	queue    chan queueEntry
	client   *http.Client
	srv      *xmpp.XmppServer
}

func (p *Plugin) initialize() error {
	dg, err := discordgo.New(p.cfg.Token)
	if err != nil {
		return err
	}

	channels := make([]channel, 0, len(p.cfg.Channels))
	for did, name := range p.cfg.Channels {
		webhook, err := findOrCreateWebhook(dg, did)
		if err != nil {
			return err
		}
		channels = append(channels, channel{
			DiscordID: did,
			RoomName:  name,
			Webhook:   webhook,
		})
	}
	p.channels = channels
	p.dg = dg
	p.queue = make(chan queueEntry, p.cfg.QueueSize)
	p.client = &http.Client{Timeout: 5 * time.Second}

	go p.queueExecutor()

	dg.AddHandler(p.dgOnMessageCreate)

	if err := dg.Open(); err != nil {
		return err
	}

	return nil
}

func (p *Plugin) dgOnMessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.Bot {
		return
	}
	if len(m.Content) == 0 || len(m.Content) > 100 {
		// Game ignores empty/large messages, return early
		return
	}

	for _, channel := range p.channels {
		if channel.DiscordID == m.ChannelID {
			err := p.handleDiscordMessage(m.Author.ID, m.Content, channel.RoomName)
			if err != nil && p.cfg.NotLinkedMessage != "" {
				_, err = s.ChannelMessageSend(m.ChannelID, p.cfg.NotLinkedMessage)
				if err != nil {
					p.log.Error().Err(err).Msg("Failed to send a message")
				}
			}
			return
		}
	}
}

func (p *Plugin) handleDiscordMessage(author, content, toRoom string) error {
	info, err := p.getInfoByDiscordID(author)
	if err != nil {
		p.log.Debug().Err(err).Msg("GetInfo failed")
		return err
	}

	sbrwMsg := sbrw.ChatMessage{
		Type:      0,
		UserID:    info.UserID,
		From:      info.Name,
		Message:   content,
		PersonaID: info.PersonaID,
	}
	sbrwMsg.CalcTimestampAndChecksum()

	xmppMsg := xmlstream.Element{
		Name: xml.Name{"jabber:client", "message"},
		Children: []xmlstream.Element{
			{
				Name: xml.Name{"jabber:client", "channel"},
				Text: "Chat_All",
			},
			{
				Name: xml.Name{"jabber:client", "body"},
				Text: sbrwMsg.String(),
			},
		},
	}
	domain := xmpp.DefaultDomain
	xmppMsg.SetAttr("to", toRoom+"@conference."+domain)
	xmppMsg.SetAttr("type", "groupchat")
	xmppMsg.SetAttr("from", fmt.Sprintf("sbrw.%d@%s/EA-Chat", info.PersonaID, domain))
	p.srv.RoutePluginMessage(xmppMsg)

	return nil
}

func (p *Plugin) OnMessage(msg *plugin.Message) {
	if !p.cfg.Enabled {
		return
	}
	if !msg.GroupChat {
		return
	}
	if msg.ByPlugin {
		return
	}

	for _, channel := range p.channels {
		if msg.To.Local == channel.RoomName {
			sbrwMsg, err := sbrw.ParseMessage(msg.Body)
			if err != nil {
				return
			}
			personaID := strings.TrimPrefix(msg.From.Local, "sbrw.")
			p.log.Debug().Msg("Queueing webhook")
			p.queueWebhook(channel.Webhook, &discordgo.WebhookParams{
				Content:         sbrwMsg.Message,
				Username:        sbrwMsg.From,
				AvatarURL:       strings.ReplaceAll(p.cfg.AvatarURL, "%", personaID),
				AllowedMentions: &discordgo.MessageAllowedMentions{},
			})
			return
		}
	}
}
