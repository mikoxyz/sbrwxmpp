// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package discord

import (
	"time"

	"github.com/bwmarrin/discordgo"
)

type queueEntry struct {
	*discordgo.Webhook
	*discordgo.WebhookParams
}

func (p *Plugin) queueWebhook(wh *discordgo.Webhook, params *discordgo.WebhookParams) {
	entry := queueEntry{
		Webhook:       wh,
		WebhookParams: params,
	}
	select {
	case p.queue <- entry:
	default:
		p.log.Warn().Msg("Dropped webhook message")
	}
}

func (p *Plugin) queueExecutor() {
	for entry := range p.queue {
		start := time.Now()
		_, err := p.dg.WebhookExecute(entry.Webhook.ID, entry.Webhook.Token, false, entry.WebhookParams)
		if err != nil {
			p.log.Error().Err(err).Msg("Failed to execute webhook")
		} else {
			p.log.Debug().Dur("duration", time.Since(start)).Msg("Executed webhook")
		}
	}
}
