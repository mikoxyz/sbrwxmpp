// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package registry

import (
	"sort"

	"github.com/redbluescreen/sbrwxmpp/plugin"
	"github.com/redbluescreen/sbrwxmpp/xmpp"
	"github.com/rs/zerolog"
)

type CreateFunc func(*xmpp.XmppServer, zerolog.Logger) (plugin.Plugin, error)

type RegisteredPlugin struct {
	Name     string
	Create   CreateFunc
	Priority int
}

var RegisteredPlugins = make([]RegisteredPlugin, 0)

func Register(name string, f CreateFunc, prio int) {
	RegisteredPlugins = append(RegisteredPlugins, RegisteredPlugin{
		Name:     name,
		Create:   f,
		Priority: prio,
	})
	sort.Slice(RegisteredPlugins, func(i, j int) bool {
		return RegisteredPlugins[i].Priority < RegisteredPlugins[j].Priority
	})
}
