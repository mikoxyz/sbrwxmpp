// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package cmdhook

import (
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/redbluescreen/sbrwxmpp/plugin"
	"github.com/redbluescreen/sbrwxmpp/plugin/registry"
	"github.com/redbluescreen/sbrwxmpp/sbrw"
	"github.com/redbluescreen/sbrwxmpp/xmpp"
	"github.com/rs/zerolog"
)

func init() {
	registry.Register("cmdhook", Create, 0)
}

type Config struct {
	Target string
	Secret string
}

func Create(srv *xmpp.XmppServer, logger zerolog.Logger) (plugin.Plugin, error) {
	var config Config
	if err := srv.Config.DecodePluginConfig("cmdhook", &config); err != nil {
		return nil, err
	}
	p := &Plugin{
		cfg: config,
		cl:  http.Client{Timeout: 2500 * time.Millisecond},
	}

	return p, nil
}

type Plugin struct {
	cfg Config
	cl  http.Client
}

func (p Plugin) OnMessage(rawMsg *plugin.Message) {
	if p.cfg.Target == "" {
		return
	}
	msg, err := sbrw.ParseMessage(rawMsg.Body)
	if err != nil {
		return
	}
	if !strings.HasPrefix(msg.Message, "/") {
		return
	}
	rawMsg.Cancelled = true
	fromID := strings.TrimPrefix(rawMsg.From.Local, "sbrw.")
	qs := url.Values{
		"pid": {fromID},
		"cmd": {msg.Message},
	}
	req, err := http.NewRequest("POST", p.cfg.Target+"?"+qs.Encode(), nil)
	if err != nil {
		return
	}
	req.Header.Add("Authorization", p.cfg.Secret)
	resp, err := p.cl.Do(req)
	if err != nil {
		return
	}
	resp.Body.Close()
	return
}
