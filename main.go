// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package main

import (
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"path"
	"strings"

	"github.com/mattn/go-colorable"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	bolt "go.etcd.io/bbolt"

	"github.com/redbluescreen/sbrwxmpp/api"
	"github.com/redbluescreen/sbrwxmpp/certgen"
	pconfig "github.com/redbluescreen/sbrwxmpp/config"
	"github.com/redbluescreen/sbrwxmpp/db"
	"github.com/redbluescreen/sbrwxmpp/plugin"
	"github.com/redbluescreen/sbrwxmpp/plugin/registry"
	"github.com/redbluescreen/sbrwxmpp/tls"
	"github.com/redbluescreen/sbrwxmpp/xmpp"

	_ "github.com/redbluescreen/sbrwxmpp/plugin/cmdhook"
	_ "github.com/redbluescreen/sbrwxmpp/plugin/discord"
)

var defaultConfig = `# Address on which the XMPP server should listen on.
addr = ":5333"

# Path to certificate and private key. If not specified, a self-signed certificate will be used.
# cert = "cert.pem"
# certkey = "key.pem"

# Maximum number of messages in client outbound queue. Keep as default unless you encounter problems.
write_queue_size = 64

[api]
# Address on which the API server should listen on.
addr = "localhost:8087"

# Key used to authenticate with the API server. Set OPENFIRE_TOKEN to this value.
key = "<<APIKEY>>"

[logging]
# Set to true to enable pretty logging instead of raw JSON logs.
pretty = false

# Logging levels
api = "warn"
xmpp = "info"

### PLUGINS ###

# Command hook plugin
# [plugins.cmdhook]
# target = "http://localhost:8080/soapbox-race-core/Engine.svc/ofcmdhook"
# secret = "<<APIKEY>>"

# Discord chat sync plugin
# [plugins.discord]
# enabled = true
# token = "Bot <bot token>"
# # API endpoint that returns avatar for a driver. % is replaced with the driver ID.
# # API must be externally available, because it will be provided to Discord.
# avatar_url = "https://chatsync-api.example.com/avatar?driver=%"
# # API endpoint that returns player information, given a Discord ID. % is replaced with Discord user ID.
# # Example response: {"personaId":100,"userId":1,"name":"MYDRIVER"}
# info_url = "https://chatsync-api.example.com/info?id=%"
# # Message that will be sent if the Discord user doesn't have a linked game account (info_url returns a non-200 status)
# not_linked_message = "⚠️ To use this chat, you must link your Discord account with your game account"
# # Outgoing webhook message queue size. You probably don't need to change it
# queue_size = 16

# Discord channel ID to game channel mappings
# [plugins.discord.channels]
# 308994132968210433 = "channel.EN__1"
`

func main() {
	config, err := pconfig.LoadConfig()
	if err != nil {
		if os.IsNotExist(err) {
			fmt.Println("No configuration found, generating")
			genConfig := strings.ReplaceAll(defaultConfig, "<<APIKEY>>", xmpp.RandomStringSecure(32))
			err = ioutil.WriteFile("sbrwxmpp.toml", []byte(genConfig), os.ModePerm)
			if err != nil {
				fmt.Printf("Failed to save config: %v\n", err)
			}
			config, err = pconfig.LoadConfig()
			if err != nil {
				fmt.Printf("Failed to read config: %v\n", err)
			}
		} else {
			fmt.Printf("Failed to read config: %v\n", err)
		}
	}

	if config.Logging.Pretty {
		log.Logger = log.Output(zerolog.ConsoleWriter{Out: colorable.NewColorableStdout()})
	}

	logger := log.With().Str("subsystem", "xmpp").Logger().Level(config.Logging.XMPP.Level)

	ln, err := net.Listen("tcp", config.Addr)
	if err != nil {
		logger.Fatal().Err(err).Msg("Failed to listen")
	}

	if config.Cert == "" || config.CertKey == "" {
		logger.Info().Msg("No certificate specified, using selfsigned certificate")
		domain := xmpp.DefaultDomain
		config.Cert = path.Join("sbrwxmpp-certs", domain+".crt")
		config.CertKey = path.Join("sbrwxmpp-certs", domain+".key")
		if _, err := os.Stat(config.Cert); os.IsNotExist(err) {
			logger.Info().Str("domain", domain).Msg("No certificate found, generating new")
			_ = os.Mkdir("sbrwxmpp-certs", 0775)
			err = certgen.GenerateCertificate("sbrwxmpp-certs", domain)
			if err != nil {
				logger.Fatal().Err(err).Msg("Failed to generate certificate")
			}
		}
	}

	cert, err := tls.LoadX509KeyPair(config.Cert, config.CertKey)
	if err != nil {
		logger.Fatal().Err(err).Msg("Failed to load certs")
	}
	tlsConfig := &tls.Config{
		Certificates: []tls.Certificate{cert},
		// Just in case, game doesn't like data being split
		// into multiple records
		DynamicRecordSizingDisabled: true,
	}

	bdb, err := bolt.Open("sbrwxmpp.db", 0664, nil)
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to open DB")
	}
	db := &db.DB{bdb}
	err = db.Initialize()
	if err != nil {
		log.Fatal().Err(err).Msg("Failed to initialize DB")
	}

	server := &xmpp.XmppServer{
		Logger:  logger,
		DB:      db,
		Config:  config,
		Clients: make(map[string]*xmpp.Client),
		Rooms:   make(map[string]*xmpp.Room),
	}

	plugins := make([]plugin.Plugin, len(registry.RegisteredPlugins))
	for i, regPlugin := range registry.RegisteredPlugins {
		logger := log.With().Str("plugin", regPlugin.Name).Logger().Level(zerolog.InfoLevel)
		logger.Info().Msg("Loading plugin")
		plugin, err := regPlugin.Create(server, logger)
		if err != nil {
			logger.Fatal().Err(err).Msg("Failed to create plugin")
		}
		plugins[i] = plugin
	}

	server.Plugins = plugins

	apiSrv := api.Server{
		XMPP:   server,
		DB:     db,
		Config: config,
		Logger: log.With().Str("subsystem", "api").Logger().Level(config.Logging.API.Level),
	}
	go apiSrv.Run()
	logger.Info().Stringer("addr", ln.Addr()).Msg("Server listening!")
	server.Run(ln, tlsConfig)
}
