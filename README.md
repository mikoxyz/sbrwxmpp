# sbrwxmpp
[![Latest Release](https://gitlab.com/sparkserver/sbrwxmpp/-/badges/release.svg)](https://gitlab.com/sparkserver/sbrwxmpp/-/releases)

A XMPP server specifically designed for Soapbox Race World.

## Motivation

Traditionally SBRW servers have used [Openfire](https://igniterealtime.org/projects/openfire) as the XMPP server. So why make a new one?

1. **TLSv1 support**  
The game uses an ancient version of OpenSSL that doesn't support newer TLS versions. Newer Java versions disable TLSv1 so one must go through hoops to re-enable it for Openfire. SBRWXMPP uses a vendored version of Go TLS library, so not even a Go update can break TLS for SBRWXMPP.

2. **Old XMPP standards**  
The game requires the XMPP server to support obsolete XMPP standards like [XEP-0078](https://xmpp.org/extensions/xep-0078.html) and [Groupchat 1.0](https://xmpp.org/extensions/xep-0045.html#enter-gc). Openfire updates may remove support for these standards. XEP-0078 is already unsupported in Openfire and requires installing a plugin to re-enable it. But SBRWXMPP is guaranteed to support features required by the game.

3. **Game's fragile XMPP client**  
The game's XMPP implementation is known to break if the XMPP handshake contains features not expected by the client. For example not disabling [Stream Compression](https://xmpp.org/extensions/xep-0138.html) and [Roster Versioning](https://xmpp.org/extensions/xep-0237.html) in Openfire breaks the game. SBRWXMPP doesn't support any unnecessary XMPP extensions that are not needed by the game.

4. **Performance**  
SBRWXMPP is written in Go, instead of Java, and is designed to be as lightweight and performant as possible.

## Usage
Download the [latest release of sbrwxmpp](https://gitlab.com/sparkserver/sbrwxmpp/-/releases/permalink/latest) (or build it yourself)  
Create a folder for sbrwxmpp, place the binary in the created folder and run it.  
A configuration file (`sbrwxmpp.toml`) will be created with other necessary files such as certificate and database.  

By default, the SBRW Core only supports Openfire API. You will need to apply [a patch for adding sbrwxmpp support](https://gitlab.com/sparkserver/core-v3/-/blob/master/patches/0003-Add-SBRWXMPP-support.patch).

You will need to do the following modifications in `PARAMETER` table:
|Parameter|Value|
|---------|-----|
|`SBRWXMPP_ADDRESS`|`http://127.0.0.1:8087/api/`|
|`SBRWXMPP_TOKEN`|value of `api.key` in `sbrwxmpp.toml`|
|`XMPP_IP`|publicly accessible IP or domain name of the XMPP server|
|`XMPP_PORT`|port of `addr` in `sbrwxmpp.toml` (5333 by default)|
|`OPENFIRE_ADDRESS`|_(delete this parameter)_|
|`OPENFIRE_TOKEN`|_(delete this parameter)_|

Make sure you change the value of `domain` in `sbrwxmpp.toml`. It should be equal to the value of `XMPP_IP` in core configuration.

## Used by
- [Sparkserver](https://sparkserver.io)
- [NightRiderz](https://nightriderz.world)
- [World Evolved](https://world-evolved.ru/en/)
