// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

package sbrw

import (
	"encoding/xml"
	"time"
)

type ChatMessage struct {
	XMLName xml.Name `xml:"ChatMsg"`

	Type     uint   `xml:"Type,attr"`
	UserID   uint64 `xml:"Uid,attr"`
	Time     int64  `xml:"Time,attr"`
	Checksum int64  `xml:"Cs,attr"`
	From     string `xml:"From"`
	Message  string `xml:"Msg"`

	PersonaID uint64 `xml:"-"`
}

func (m *ChatMessage) CalcTimestampAndChecksum() {
	m.Time = getTimestamp()

	hsh := binhash64(m.From, 0xFFFFFFFFFFFFFFFF)
	hsh = binhash64(m.Message, m.UserID^m.PersonaID^hsh)
	hsh = uint64(m.Type) ^ hsh ^ uint64(m.Time)
	m.Checksum = int64(hsh)
}

func (m *ChatMessage) String() string {
	bytes, err := xml.Marshal(m)
	if err != nil {
		panic(err)
	}
	return string(bytes)
}

func ParseMessage(body string) (*ChatMessage, error) {
	msg := new(ChatMessage)
	if err := xml.Unmarshal([]byte(body), msg); err != nil {
		return nil, err
	}
	return msg, nil
}

func getTimestamp() int64 {
	// We shift the timestamp forward by the server timezone offset
	// Yes, it's weird, but it's needed for the messages to show up
	currentTime := time.Now()
	_, offset := currentTime.Zone()
	currentTime = currentTime.Add(time.Duration(offset) * time.Second)

	filetime := uint64(currentTime.UnixNano()/100 + 116444736000000000)
	tsh := uint32(filetime >> 32)
	tsl := uint32(filetime)
	tshpd := (^tsh) * 0x57A5DEEB
	tslpd := (^tsl) * 0x57A5DEEB
	return int64(tshpd)<<32 | int64(tslpd)
}

func binhash64(str string, init uint64) uint64 {
	for _, c := range []byte(str) {
		init *= 33
		init += uint64(c)
	}
	return init
}
